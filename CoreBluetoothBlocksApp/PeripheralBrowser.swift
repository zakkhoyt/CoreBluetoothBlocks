//
//  BLEBrowser.swift
//  BLECallbackApp
//
//  Created by Zakk Hoyt on 4/26/18.
//  Copyright © 2018 Zakk Hoyt. All rights reserved.
//

import Foundation
import CoreBluetooth
import CoreBluetoothBlocks

class PeripheralBrowser {
    private let centralManager: CBCentralManager
    private let centralManagerDelegate = CBCentralManagerBlockDelegate()
    
    private var peripheral: CBPeripheral?
    private let peripheralDelegate = CBPeripheralBlockDelegate()
    
    init() {
        centralManager = CBCentralManager(delegate: centralManagerDelegate, queue: DispatchQueue(label: "CoreBluetooth"))
        centralManagerDelegate.didUpdateState(completion: { [weak self] (central) in
            print("state: \(central.state.rawValue)")
            switch central.state {
            case .poweredOn:
                self?.startScan()
            default:
                break
            }
        })
    }
    
    private func startScan() {
        centralManagerDelegate.didDiscoverPeripheral { [weak self] (central, peripheral, advertisementData, rssi) in
            print("didDiscoverPeripheral: \(peripheral.identifier)")
            
            /// Uncomment this to connect to a specific device. Else connect to first one
//            if peripheral.identifier.uuidString == "DB417198-7C6C-A2A6-F286-40B4431DC610" {
                self?.connect(to: peripheral)
//            }
        }
        centralManager.scanForPeripherals(withServices: nil, options: nil)
    }
    
    private func connect(to peripheral: CBPeripheral) {
        self.peripheral = peripheral
        centralManager.stopScan()
        
        centralManagerDelegate.didConnectToPeripheral { [weak self] (centralManager, peripheral) in
            print("""
                Did connect to peripheral:
                \(peripheral.description)
                """)

            self?.discoverServices(for: peripheral)
            self?.disconnect(from: peripheral, after: 5)
        }
        centralManagerDelegate.didFailToConnect { (centralManager, peripheral, error) in
            print("""
                Did fail to connect to peripheral:
                \(peripheral.description)
                with error:
                \(error?.localizedDescription)
                """)
        }
        centralManager.connect(peripheral, options: nil)
    }
    
    private func discoverServices(for peripheral: CBPeripheral) {
        print("Discovering services")
        peripheral.delegate = peripheralDelegate
        peripheralDelegate.didDiscoverServices { [weak self] (peripheral, error) in
            if let error = error {
                print("error while discovering services: \(error.localizedDescription)")
                return
            }
            guard let services = peripheral.services else {
                print("No services discovered")
                return
            }
            for service in services {
                print("Discovered service: \(service.description)")
                self?.discoverCharacteristics(for: service)
            }
        }
        peripheral.discoverServices(nil)
    }
    
    private func discoverCharacteristics(for service: CBService) {
        print("Discovering characteristics")
        peripheralDelegate.didDiscoverCharacteristics { (peripheral, service, error) in
            if let error = error {
                print("error while discovering characteristics: \(error.localizedDescription)")
                return
            }
            guard let characteristics = service.characteristics else {
                print("No characteristics discovered for service \(service.description)")
                return
            }
            for characteristic in characteristics {
                print("Discovered characteristic: \(characteristic.description)")
            }
        }
        service.peripheral.discoverCharacteristics([], for: service)
    }
    
    private func disconnect(from peripheral: CBPeripheral, after seconds: TimeInterval) {
        DispatchQueue.global().asyncAfter(deadline: .now() + seconds, execute: { [weak self] in
            self?.performDisconnect(from: peripheral)
        })
    }
    
    private func performDisconnect(from peripheral: CBPeripheral) {
        centralManagerDelegate.didDisconnect { (centralManager, peripheral, error) in
            print("""
                Did disconnect from peripheral:
                \(peripheral.description)
                """)
        }
        print("Requesting disconnect from peripheral")
        centralManager.cancelPeripheralConnection(peripheral)
    }
}
