//
//  ViewController.swift
//  BLECallbackApp
//
//  Created by Zakk Hoyt on 4/26/18.
//  Copyright © 2018 Zakk Hoyt. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    private var bleBrowser: PeripheralBrowser?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        bleBrowser = PeripheralBrowser()
    }
}

