//
//  CBPeripheralBlockDelegate.swift
//  CoreBluetoothBlocks
//
//  Created by Zakk Hoyt on 4/26/18.
//  Copyright © 2018 Zakk Hoyt. All rights reserved.
//

import Foundation
import CoreBluetooth

public typealias PeripheralDidUpdateName = (_ peripheral: CBPeripheral) -> Void
public typealias PeripheralDidModifyServices = (_ peripheral: CBPeripheral, _ invalidatedServices: [CBService]) -> Void
public typealias PeripheralDidUpdateRSSI = (_ peripheral: CBPeripheral, _ error: Error?) -> Void
public typealias PeripheralDidReadRSSI = (_ peripheral: CBPeripheral, _ RSSI: NSNumber, _ error: Error?) -> Void
public typealias PeripheralDidDiscoverServices = (_ peripheral: CBPeripheral, _ error: Error?) -> Void
public typealias PeripheralDidDiscoverIncludedServices = (_ peripheral: CBPeripheral, _ service: CBService, _ error: Error?) -> Void
public typealias PeripheralDidDiscoverCharacteristics = (_ peripheral: CBPeripheral, _ service: CBService, _ error: Error?) -> Void
public typealias PeripheralDidUpdateValueForCharacteristics = (_ peripheral: CBPeripheral, _ characteristic: CBCharacteristic, _ error: Error?) -> Void
public typealias PeripheralDidWriteValueForCharacteristics = (_ peripheral: CBPeripheral, _ characteristic: CBCharacteristic, _ error: Error?) -> Void
public typealias PeripheralDidUpdateNotificationState = (_ peripheral: CBPeripheral, _ characteristic: CBCharacteristic, _ error: Error?) -> Void
public typealias PeripheralDidDiscoverDescriptors = (_ peripheral: CBPeripheral, _ characteristic: CBCharacteristic, _ error: Error?) -> Void
public typealias PeripheralDidUpdateValueForDescriptor = (_ peripheral: CBPeripheral, _ descriptor: CBDescriptor, _ error: Error?) -> Void
public typealias PeripheralDidWriteValueForDescriptor = (_ peripheral: CBPeripheral, _ descriptor: CBDescriptor, _ error: Error?) -> Void
public typealias PeripheralIsReadyDoSendWriteWithoutResponse = (_ peripheral: CBPeripheral) -> Void
public typealias PeripheralDidOpenCBL2CAPChannel = (_ peripheral: CBPeripheral, _ channel: CBL2CAPChannel?, _ error: Error?) -> Void

public class CBPeripheralBlockDelegate: NSObject {
    private var _peripheralDidUpdateName: PeripheralDidUpdateName?
    private var _peripheralDidModifyServices: PeripheralDidModifyServices?
    private var _peripheralDidUpdateRSSI: PeripheralDidUpdateRSSI?
    private var _peripheralDidReadRSSI: PeripheralDidReadRSSI?
    private var _peripheralDidDiscoverServices: PeripheralDidDiscoverServices?
    private var _peripheralDidDiscoverIncludedServices: PeripheralDidDiscoverIncludedServices?
    private var _peripheralDidDiscoverCharacteristics: PeripheralDidDiscoverCharacteristics?
    private var _peripheralDidUpdateValueForCharacteristics: PeripheralDidUpdateValueForCharacteristics?
    private var _peripheralDidWriteValueForCharacteristics: PeripheralDidWriteValueForCharacteristics?
    private var _peripheralDidUpdateNotificationState: PeripheralDidUpdateNotificationState?
    private var _peripheralDidDiscoverDescriptors: PeripheralDidDiscoverDescriptors?
    private var _peripheralDidUpdateValueForDescriptor: PeripheralDidUpdateValueForDescriptor?
    private var _peripheralDidWriteValueForDescriptor: PeripheralDidWriteValueForDescriptor?
    private var _peripheralIsReadyDoSendWriteWithoutResponse: PeripheralIsReadyDoSendWriteWithoutResponse?
    private var _peripheralDidOpenCBL2CAPChannel: PeripheralDidOpenCBL2CAPChannel?

    /*!
     *  @method peripheralDidUpdateName:
     *
     *  @param peripheral    The peripheral providing this update.
     *
     *  @discussion            This method is invoked when the @link name @/link of <i>peripheral</i> changes.
     */
    public func didUpdateName(completion: @escaping PeripheralDidUpdateName) {
        _peripheralDidUpdateName = completion
    }
    
    /*!
     *  @method peripheral:didModifyServices:
     *
     *  @param peripheral            The peripheral providing this update.
     *  @param invalidatedServices    The services that have been invalidated
     *
     *  @discussion            This method is invoked when the @link services @/link of <i>peripheral</i> have been changed.
     *                        At this point, the designated <code>CBService</code> objects have been invalidated.
     *                        Services can be re-discovered via @link discoverServices: @/link.
     */
    public func didModifyServices(completion: @escaping PeripheralDidModifyServices) {
        _peripheralDidModifyServices = completion
    }
    
    /*!
     *  @method peripheralDidUpdateRSSI:error:
     *
     *  @param peripheral    The peripheral providing this update.
     *    @param error        If an error occurred, the cause of the failure.
     *
     *  @discussion            This method returns the result of a @link readRSSI: @/link call.
     *
     *  @deprecated            Use {@link peripheral:didReadRSSI:error:} instead.
     */
    public func didUpdateRSSI(completion: @escaping PeripheralDidUpdateRSSI) {
        _peripheralDidUpdateRSSI = completion
    }
    
    /*!
     *  @method peripheral:didReadRSSI:error:
     *
     *  @param peripheral    The peripheral providing this update.
     *  @param RSSI            The current RSSI of the link.
     *  @param error        If an error occurred, the cause of the failure.
     *
     *  @discussion            This method returns the result of a @link readRSSI: @/link call.
     */
    public func didReadRSSI(completion: @escaping PeripheralDidReadRSSI) {
        _peripheralDidReadRSSI = completion
    }
    
    /*!
     *  @method peripheral:didDiscoverServices:
     *
     *  @param peripheral    The peripheral providing this information.
     *    @param error        If an error occurred, the cause of the failure.
     *
     *  @discussion            This method returns the result of a @link discoverServices: @/link call. If the service(s) were read successfully, they can be retrieved via
     *                        <i>peripheral</i>'s @link services @/link property.
     *
     */
    public func didDiscoverServices(completion: @escaping PeripheralDidDiscoverServices) {
        _peripheralDidDiscoverServices = completion
    }
    
    /*!
     *  @method peripheral:didDiscoverIncludedServicesForService:error:
     *
     *  @param peripheral    The peripheral providing this information.
     *  @param service        The <code>CBService</code> object containing the included services.
     *    @param error        If an error occurred, the cause of the failure.
     *
     *  @discussion            This method returns the result of a @link discoverIncludedServices:forService: @/link call. If the included service(s) were read successfully,
     *                        they can be retrieved via <i>service</i>'s <code>includedServices</code> property.
     */
    public func didDiscoverIncludedServices(completion: @escaping PeripheralDidDiscoverIncludedServices) {
        _peripheralDidDiscoverIncludedServices = completion
    }
    
    /*!
     *  @method peripheral:didDiscoverCharacteristicsForService:error:
     *
     *  @param peripheral    The peripheral providing this information.
     *  @param service        The <code>CBService</code> object containing the characteristic(s).
     *    @param error        If an error occurred, the cause of the failure.
     *
     *  @discussion            This method returns the result of a @link discoverCharacteristics:forService: @/link call. If the characteristic(s) were read successfully,
     *                        they can be retrieved via <i>service</i>'s <code>characteristics</code> property.
     */
    public func didDiscoverCharacteristics(completion: @escaping PeripheralDidDiscoverCharacteristics) {
        _peripheralDidDiscoverCharacteristics = completion
    }
    
    /*!
     *  @method peripheral:didUpdateValueForCharacteristic:error:
     *
     *  @param peripheral        The peripheral providing this information.
     *  @param characteristic    A <code>CBCharacteristic</code> object.
     *    @param error            If an error occurred, the cause of the failure.
     *
     *  @discussion                This method is invoked after a @link readValueForCharacteristic: @/link call, or upon receipt of a notification/indication.
     */
    public func didUpdateValueForCharacteristics(completion: @escaping PeripheralDidUpdateValueForCharacteristics) {
        _peripheralDidUpdateValueForCharacteristics = completion
    }
    
    /*!
     *  @method peripheral:didWriteValueForCharacteristic:error:
     *
     *  @param peripheral        The peripheral providing this information.
     *  @param characteristic    A <code>CBCharacteristic</code> object.
     *    @param error            If an error occurred, the cause of the failure.
     *
     *  @discussion                This method returns the result of a {@link writeValue:forCharacteristic:type:} call, when the <code>CBCharacteristicWriteWithResponse</code> type is used.
     */
    public func didWriteValueForCharacteristics(completion: @escaping PeripheralDidWriteValueForCharacteristics) {
        _peripheralDidWriteValueForCharacteristics = completion
    }
    
    /*!
     *  @method peripheral:didUpdateNotificationStateForCharacteristic:error:
     *
     *  @param peripheral        The peripheral providing this information.
     *  @param characteristic    A <code>CBCharacteristic</code> object.
     *    @param error            If an error occurred, the cause of the failure.
     *
     *  @discussion                This method returns the result of a @link setNotifyValue:forCharacteristic: @/link call.
     */
    public func didUpdateNotificationState(completion: @escaping PeripheralDidUpdateNotificationState) {
        _peripheralDidUpdateNotificationState = completion
    }
    
    /*!
     *  @method peripheral:didDiscoverDescriptorsForCharacteristic:error:
     *
     *  @param peripheral        The peripheral providing this information.
     *  @param characteristic    A <code>CBCharacteristic</code> object.
     *    @param error            If an error occurred, the cause of the failure.
     *
     *  @discussion                This method returns the result of a @link discoverDescriptorsForCharacteristic: @/link call. If the descriptors were read successfully,
     *                            they can be retrieved via <i>characteristic</i>'s <code>descriptors</code> property.
     */
    public func didDiscoverDescriptors(completion: @escaping PeripheralDidDiscoverDescriptors) {
        _peripheralDidDiscoverDescriptors = completion
    }
    
    /*!
     *  @method peripheral:didUpdateValueForDescriptor:error:
     *
     *  @param peripheral        The peripheral providing this information.
     *  @param descriptor        A <code>CBDescriptor</code> object.
     *    @param error            If an error occurred, the cause of the failure.
     *
     *  @discussion                This method returns the result of a @link readValueForDescriptor: @/link call.
     */
    public func didUpdateValueForDescriptor(completion: @escaping PeripheralDidUpdateValueForDescriptor) {
        _peripheralDidUpdateValueForDescriptor = completion
    }
    
    /*!
     *  @method peripheral:didWriteValueForDescriptor:error:
     *
     *  @param peripheral        The peripheral providing this information.
     *  @param descriptor        A <code>CBDescriptor</code> object.
     *    @param error            If an error occurred, the cause of the failure.
     *
     *  @discussion                This method returns the result of a @link writeValue:forDescriptor: @/link call.
     */
    public func didWriteValueForDescriptor(completion: @escaping PeripheralDidWriteValueForDescriptor) {
        _peripheralDidWriteValueForDescriptor = completion
    }
    
    /*!
     *  @method peripheralIsReadyToSendWriteWithoutResponse:
     *
     *  @param peripheral   The peripheral providing this update.
     *
     *  @discussion         This method is invoked after a failed call to @link writeValue:forCharacteristic:type: @/link, when <i>peripheral</i> is again
     *                      ready to send characteristic value updates.
     *
     */
    public func isReadyDoSendWriteWithoutResponse(completion: @escaping PeripheralIsReadyDoSendWriteWithoutResponse) {
        _peripheralIsReadyDoSendWriteWithoutResponse = completion
    }
    /*!
     *  @method peripheral:didOpenL2CAPChannel:error:
     *
     *  @param peripheral        The peripheral providing this information.
     *  @param channel            A <code>CBL2CAPChannel</code> object.
     *    @param error            If an error occurred, the cause of the failure.
     *
     *  @discussion                This method returns the result of a @link openL2CAPChannel: @link call.
     */
    public func didOpenCBL2CAPChannel(completion: @escaping PeripheralDidOpenCBL2CAPChannel) {
        _peripheralDidOpenCBL2CAPChannel = completion
    }

}

extension CBPeripheralBlockDelegate: CBPeripheralDelegate {
    public func peripheralDidUpdateName(_ peripheral: CBPeripheral) {
        _peripheralDidUpdateName?(peripheral)
    }

    public func peripheral(_ peripheral: CBPeripheral, didModifyServices invalidatedServices: [CBService]) {
        _peripheralDidModifyServices?(peripheral, invalidatedServices)
    }

    public func peripheralDidUpdateRSSI(_ peripheral: CBPeripheral, error: Error?) {
        _peripheralDidUpdateRSSI?(peripheral, error)
    }

    public func peripheral(_ peripheral: CBPeripheral, didReadRSSI RSSI: NSNumber, error: Error?) {
        _peripheralDidReadRSSI?(peripheral, RSSI, error)
    }

    public func peripheral(_ peripheral: CBPeripheral, didDiscoverServices error: Error?) {
        _peripheralDidDiscoverServices?(peripheral, error)
    }

    public func peripheral(_ peripheral: CBPeripheral, didDiscoverIncludedServicesFor service: CBService, error: Error?) {
        _peripheralDidDiscoverIncludedServices?(peripheral, service, error)
    }

    public func peripheral(_ peripheral: CBPeripheral, didDiscoverCharacteristicsFor service: CBService, error: Error?) {
        _peripheralDidDiscoverCharacteristics?(peripheral, service, error)
    }

    public func peripheral(_ peripheral: CBPeripheral, didUpdateValueFor characteristic: CBCharacteristic, error: Error?) {
        _peripheralDidUpdateValueForCharacteristics?(peripheral, characteristic, error)
    }

    public func peripheral(_ peripheral: CBPeripheral, didWriteValueFor characteristic: CBCharacteristic, error: Error?) {
        _peripheralDidWriteValueForCharacteristics?(peripheral, characteristic, error)
    }

    public func peripheral(_ peripheral: CBPeripheral, didUpdateNotificationStateFor characteristic: CBCharacteristic, error: Error?) {
        _peripheralDidUpdateNotificationState?(peripheral, characteristic, error)
    }

    public func peripheral(_ peripheral: CBPeripheral, didDiscoverDescriptorsFor characteristic: CBCharacteristic, error: Error?) {
        _peripheralDidDiscoverDescriptors?(peripheral, characteristic, error)
    }

    public func peripheral(_ peripheral: CBPeripheral, didUpdateValueFor descriptor: CBDescriptor, error: Error?) {
        _peripheralDidUpdateValueForDescriptor?(peripheral, descriptor, error)
    }

    public func peripheral(_ peripheral: CBPeripheral, didWriteValueFor descriptor: CBDescriptor, error: Error?) {
        _peripheralDidWriteValueForDescriptor?(peripheral, descriptor, error)
    }

    public func peripheralIsReady(toSendWriteWithoutResponse peripheral: CBPeripheral) {
        _peripheralIsReadyDoSendWriteWithoutResponse?(peripheral)
    }

    public func peripheral(_ peripheral: CBPeripheral, didOpen channel: CBL2CAPChannel?, error: Error?) {
        _peripheralDidOpenCBL2CAPChannel?(peripheral, channel, error)
    }
}
