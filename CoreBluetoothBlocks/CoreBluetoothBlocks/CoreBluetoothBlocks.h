//
//  CoreBluetoothBlocks.h
//  CoreBluetoothBlocks
//
//  Created by Zakk Hoyt on 4/26/18.
//  Copyright © 2018 Zakk Hoyt. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for CoreBluetoothBlocks.
FOUNDATION_EXPORT double CoreBluetoothBlocksVersionNumber;

//! Project version string for CoreBluetoothBlocks.
FOUNDATION_EXPORT const unsigned char CoreBluetoothBlocksVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <CoreBluetoothBlocks/PublicHeader.h>


